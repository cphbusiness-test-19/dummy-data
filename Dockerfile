FROM node:11

# Create app directory
WORKDIR /usr/src/app

COPY . .

RUN npm install


CMD ["node", "mongoose.js"]
