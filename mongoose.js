const mongoose = require('mongoose');
const fs = require('fs');
const bcrypt = require('bcrypt');

mongoose.Promise = Promise;

mongoose.connect(process.env.databaseurl || "mongodb://mongo/les", {useNewUrlParser: true});


let models = {
    Course: {
        CourseName: String,
        Room: String,
        TeacherId: Number,
        StudentsId: Array,
        Category: String,
        NoHoursPrWeek: Number,
        Weekdays: String,
        Description: String,
        Semester: String,
        Language: String,
        Level: String,
        Visible: Boolean,
        Archived: Boolean,
        Votes: {type: Number, default: 0}
    },
    AdminUser: {
        Name: String,
        Password: String,
        UserName: String,
        Email: String,
        Phone: String,
        Public: Boolean,
        Active: Number,
        Title: String,
        Token: String,
        Registered: Array,
    },
    Student: {
        Name: String,
    },
    Teacher: {
        Name: String,
        Candidate: Boolean,
    },
    Votes: {
        CourseIds: [{ type: mongoose.Schema.Types.ObjectId, ref: 'Course' }],
        Active: Boolean,
        CreatedAt: {type: Date, default: Date.now}
    }
};


(async () => {

    for (let model of Object.keys(models)) {
        mongoose.model(model, models[model]);

        let data = JSON.parse(fs.readFileSync(model + ".json"));

        for (let i = 0; i < data.length; i++) {
            if(typeof data[i].Password !== "undefined"){
                data[i].Password = bcrypt.hashSync(data[i].Password, bcrypt.genSaltSync(10));
            }
        }


        await mongoose.model(model).insertMany(data);

        console.log(data.length + " rows inserted into " + model)
    }

    await mongoose.model("AdminUser").insertMany([{
        "Name": "Test user",
        "Password": bcrypt.hashSync("test", bcrypt.genSaltSync(10)),
        "UserName": "test",
        "Email": "test@test.dk",
        "Phone": "+4500000000",
        "Public": true,
        "Active": true,
        "Title": "Teacher",
        "Token": "",
        "Registered": [
        ]
    }])

    process.exit()
})();
